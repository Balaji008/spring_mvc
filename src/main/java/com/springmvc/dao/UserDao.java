package com.springmvc.dao;

import java.io.Serializable;

import javax.transaction.Transactional;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.NativeQuery;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.orm.hibernate5.HibernateTemplate;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.EnableTransactionManagement;

import com.springmvc.model.User;

@Repository
@Transactional
public class UserDao {

	@Autowired
	private SessionFactory factory;

	public int saveUser(User user) {
		Session session = factory.getCurrentSession();
//		NativeQuery createNativeQuery = session.createNativeQuery("insert into users(name,email,password) values('"+user.getName()+"','"+user.getEmail()+"','"+user.getPassword()+"')");
		session.save(user);
		return 0;
	}
}
