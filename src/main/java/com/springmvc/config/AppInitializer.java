package com.springmvc.config;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;

import org.springframework.web.servlet.support.AbstractAnnotationConfigDispatcherServletInitializer;

public class AppInitializer extends AbstractAnnotationConfigDispatcherServletInitializer {
	// private int maxUploadSizeInMb = 5 * 1024 * 1024; // 5 MB
	@Override
	protected Class<?>[] getRootConfigClasses() {
		return new Class[] { HibernateConfiguration.class };
	}

	@Override
	protected String[] getServletMappings() {
		return new String[] { "/" };
	}

	@Override
	protected Class<?>[] getServletConfigClasses() {
		// TODO Auto-generated method stub
		return null;
	}

	/*
	 * @Override protected void customizeRegistration(ServletRegistration.Dynamic
	 * registration) {
	 * 
	 * // upload temp file will put here File uploadDirectory = new
	 * File(System.getProperty("java.io.tmpdir"));
	 * 
	 * // register a MultipartConfigElement MultipartConfigElement
	 * multipartConfigElement = new
	 * MultipartConfigElement(uploadDirectory.getAbsolutePath(), maxUploadSizeInMb,
	 * maxUploadSizeInMb * 2, maxUploadSizeInMb / 2);
	 * 
	 * registration.setMultipartConfig(multipartConfigElement);
	 * 
	 * }
	 */
}