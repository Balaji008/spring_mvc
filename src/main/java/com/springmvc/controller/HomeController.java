package com.springmvc.controller;

import java.time.LocalDateTime;
import java.util.*;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

@Controller
public class HomeController {

	@RequestMapping("/home")
	public String home(Model model) {
		model.addAttribute("name", "Balaji Gosavi");
		List<String> list = new ArrayList<String>(Arrays.asList("Balaji", "Vishal", "Vaibhav"));
		model.addAttribute("list", list);
		System.out.println("This is home url");
		return "index";
	}

	@RequestMapping("/about")
	public String about() {
		return "about";
	}

	// @RequestMapping(path="/help",method=RequestMethod.POST)
	@RequestMapping("/help")
	public ModelAndView help() {
		ModelAndView modelAndView = new ModelAndView();
		modelAndView.addObject("name", "Balaji");
		LocalDateTime localDateTime = LocalDateTime.now();
		modelAndView.addObject("time", localDateTime);

		List<Integer> numbers = Arrays.asList(1, 2, 3, 4, 4, 5, 5, 5, 5, 5, 5, 9);
		modelAndView.addObject("numbers", numbers);
		modelAndView.setViewName("help");
		return modelAndView;
	}
}
