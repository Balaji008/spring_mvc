package com.springmvc.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class RedirectController {

	@RequestMapping("/first")
	public String firstUrl() {
		System.out.println("This is first url");
		return "";
	}

	@RequestMapping("/second")
	public String secondUrl() {
		System.out.println("This is second url");
		return "";
	}
}
