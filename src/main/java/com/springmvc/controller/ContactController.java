package com.springmvc.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import com.springmvc.model.User;
import com.springmvc.service.UserService;

@Controller
public class ContactController {

	@Autowired
	private UserService UserService;

	@GetMapping("/contact")
	public String contact(Model model) {
		// model.addAttribute("Header", "Balaji");
		System.out.println("Showing form");
		return "contact";
	}

	// @PostMapping("/processform")
	@RequestMapping(path = "/processform", method = RequestMethod.POST)
	public String processForm(@ModelAttribute User user, ModelAndView model) {

		System.out.println(user);
		this.UserService.saveUser(user);
		// model.setViewName("success");
		System.out.println("processing form");
		return "success";
	}

	@ModelAttribute
	public void commonDataForModel(Model model) {
		model.addAttribute("Header", "Registration Form");
		System.out.println("Adding common data to model");
	}
	// // @PostMapping("/processform")
	// @RequestMapping(path = "/processform", method = RequestMethod.POST)
	// public ModelAndView processForm(@RequestParam("email") String email,
	// @RequestParam("name") String name,
	// @RequestParam("password") String password, ModelAndView model) {
	//

	// User user = new User();
	//
	// user.setName(name);
	// user.setEmail(email);
	// user.setPassword(password);
	//
	// model.addObject("user", user);
	// // model.addObject("name", name);
	// // model.addObject("email", email);
	// // model.addObject("password", password);
	// System.out.println(user);
	// model.setViewName("success");
	// return model;
	// }
}
