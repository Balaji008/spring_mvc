<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ page import="java.time.*"%>
<%@page isELIgnored="false"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1" />
<title>Help page</title>
</head>
<body>
	<h1>This is help page</h1>

	<%-- <%
		LocalDateTime time = (LocalDateTime) request.getAttribute("time");
	%> --%>
	<h1>
		Current date and time is
		<%-- <%=time%> --%>
		${time}
	</h1>
	<hr>
	<c:forEach var="item" items="${numbers}">
		<h1>${item }</h1>
	</c:forEach>
</body>
</html>
