<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ page import="java.util.*"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Insert title here</title>
</head>
<body>
	<h1>This is home page</h1>
	<h1>Called by home controller</h1>
	<h1>url / home</h1>

	<%
		String name = (String) request.getAttribute("name");
		List<String> names = (List<String>) request.getAttribute("list");
	%>
	<h1>
		Name is
		<%=name%></h1>

	<%
		for (String n : names) {
	%>

	<h1><%=n%></h1>

	<%
		}
	%>
</body>
</html>